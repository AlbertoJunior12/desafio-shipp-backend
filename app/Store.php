<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Location\Coordinate;
use Location\Distance\Vincenty;

class Store extends Model
{
    protected $table = 'store';

    protected $fillable = [
    	'county',
		'licenseNumber',
		'operationType',
		'establishmentType',
		'entityName',
		'dbaName',
		'streetNumber',
		'streetName',
		'city',
		'state',
		'zipCode',
		'squareFootage',
		'latitude',
		'longitude'
    ];

    public static function getFilterStores($lat, $lon) {
    	$stores = Store::all();
        $dataItems = [];
        foreach ($stores as $value) {
            $coordinate1 = new Coordinate((Float)$value['latitude'], (Float)$value['longitude']);
            $coordinate2 = new Coordinate($lat, $lon);
            $calculator = new Vincenty();

            $retorno = $calculator->getDistance($coordinate1, $coordinate2);
            if($retorno <= 65000) {
                array_push($dataItems, $value);
            }
        }
        return $dataItems;
    }
}
