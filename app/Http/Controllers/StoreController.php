<?php

namespace App\Http\Controllers;
use App\Imports\StoresImport;
use App\Store;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function importStores() {
        Excel::import(new StoresImport, 'stores.csv');
        return response()->json(['message' => 'Dados importados com sucesso!']);
    }

    public function getAllStores() {
        return Store::all();
    }

    public function list(Request $request) {

        $lat = (Float) $request->lat;
        $lon = (Float) $request->lon;
        $result = Store::getFilterStores($lat, $lon);
        return response($result, '200')
        ->header('Content-Type', 'application/json');
    }

    //
}
