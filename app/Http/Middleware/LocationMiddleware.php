<?php

namespace App\Http\Middleware;

use Closure;

class LocationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->lat && $request->lon)
            return $next($request);
        return response(['error' => 'Informe a Latitude e a Longitude'], '400')
        ->header('Content-Type', 'application/json');
    }
}
