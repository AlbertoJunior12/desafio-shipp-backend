<?php

namespace App\Imports;

use App\Store;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class StoresImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $latitude = null;
        $longitude = null;
        if($row['location']) {
            preg_match_all('/(-?\d+\.\d+)/', $row['location'], $resultado);
            $latitude = $resultado[0][0] ?? null;
            $longitude = $resultado[0][1] ?? null;
        }

        
        return new Store([
            'county' => $row["county"],
            'licenseNumber' => $row["license_number"],
            'operationType' => $row["operation_type"],
            'establishmentType' => $row["establishment_type"],
            'entityName' => $row["entity_name"],
            'dbaName' => $row["dba_name"],
            'streetNumber' => $row["street_number"],
            'streetName' => $row["street_name"],
            'city' => $row["city"],
            'state' => $row["state"],
            'zipCode' => $row["zip_code"],
            'squareFootage' => $row["square_footage"],
            'latitude' => $latitude,
            'longitude' => $longitude,
        ]);
    }
}
