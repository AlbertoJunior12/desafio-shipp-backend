<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableStore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Store', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('county')->nullable();
            $table->string('licenseNumber')->nullable();
            $table->string('operationType')->nullable();
            $table->string('establishmentType')->nullable();
            $table->string('entityName')->nullable();
            $table->string('dbaName')->nullable();
            $table->string('streetNumber')->nullable();
            $table->string('streetName')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zipCode')->nullable();
            $table->string('squareFootage')->nullable();
            $table->float('latitude', 7, 2)->nullable();
            $table->float('longitude', 7, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Store');
    }
}
