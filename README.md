# Desafio | Francisco Alberto

# Passos

- Para instalar as dependências: composer install
- Para configurar o banco de dados: Criar arquivo .env baseado no arquivo .env.example e criar o arquivo database.sqlite na pasta database
- Para migrar o banco de dados: php artisan migrate
- Para inicializar o servidor: php artisan serve
- Para acessar a api: localhost:8000
- Para carregar os dados do .csv enviar uma requisição via POST para: /import
- Para realizar a listagem das lojas enviar requisição via GET para: /V1/stores?lat=-73.700539&lon=42.754424